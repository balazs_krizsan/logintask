<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableAttempts extends Migration
{
    public function up()
    {
        Schema::create('attempts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()
                ->references('id')->on('esc_useres')
                ->onDelete('cascade');
            $table->text('hash');
            $table->timestamp('completed_on')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('attempts');
    }
}
