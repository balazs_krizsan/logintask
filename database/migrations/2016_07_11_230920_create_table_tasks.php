<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTasks extends Migration
{

    public function up()
    {
        Schema::create('tasks', function (Blueprint $table) {
            $table->increments('id');
            $table->json('taskdata');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('tasks');
    }
}
