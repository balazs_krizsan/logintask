var gulp = require('gulp');
var sass = require('gulp-sass');

gulp.task('styles', function() {
    gulp.src('resources/sass/**/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('./public/sass/'))
});

//Watch task
gulp.task('default', ['styles'], function() {
    gulp.watch('resources/sass/**/*.scss', ['styles']);
});