<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    public function home()
    {
        return view('home.index');
    }

    public function memberHome()
    {
        return view('home.memberIndex', ['user' => \EscUsers::getCurrent()]);
    }
}
