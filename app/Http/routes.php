<?php

Route::group([
    'middlewareGroups' => ['web']
], function() {

    Route::get('/', 'HomeController@home');

});

Route::group([
    'middlewareGroups' => ['web'],
    'middleware' => ['escauth']
], function() {

    Route::get('/home', 'HomeController@memberHome');

});
