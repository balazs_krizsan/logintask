<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EscuserInit extends Migration
{
    public function up()
    {
        Schema::create('esc_users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->unique();
            $table->string('email')->unique();
            $table->string('password');
            $table->boolean('activated')->default(0);
            $table->rememberToken();
            $table->timestamps();
        });

        Schema::create('esc_users_email_validation', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()
                ->references('id')->on('esc_useres')->onDelete('cascade');
            $table->string('hash');
            $table->boolean('used')->default(0);
            $table->timestamps();
        });
        Schema::create('esc_users_login_attempts', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('log_type');
            $table->text('data');
            $table->integer('attempts')->unsigned()->default(1);
            $table->ipAddress('request_address');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('esc_users');
        Schema::drop('esc_users_email_validation');
        Schema::drop('esc_users_login_attempts');
    }
}
