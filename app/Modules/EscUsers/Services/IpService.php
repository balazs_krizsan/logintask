<?php

namespace App\Modules\EscUsers\Services;

class IpService
{
    public static function ipToMaskedNetworkIp($ip, $maskBit)
    {
        return long2ip((ip2long($ip)) & ((-1 << (32 - (int)$maskBit)))).'/'.$maskBit;
    }
}
