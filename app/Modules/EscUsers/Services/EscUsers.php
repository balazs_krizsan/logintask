<?php

namespace App\Modules\EscUsers\Services;

use App\Modules\EscUsers\Models\Users;
use App\Modules\EscUsers\Services\SessionService;

class EscUsers
{
    public static function isLoggedIn()
    {
        return LoginService::isLoggedIn();
    }

    public static function isLoggedInForView()
    {
        return LoginService::isLoggedInForView();
    }

    public static function getUrl($key)
    {
        $urls = [
            'login' => '/users/login',
            'logout' => '/users/logout',
            'registration' => '/users/registration',
            'captchaRevalidation' => '/users/captcha-revalidation',
        ];
        return $urls[$key];
    }

    public static function getCurrent()
    {
        return Users::where('id', SessionService::getLoggedInUserId())->firstorfail();
    }

    public static function getCurrentId()
    {
        return static::getCurrent()->id;
    }

    public static function needCaptcha()
    {
        return LoginService::needCaptcha();
    }

    public static function needCaptchaRevalidation()
    {
        return LoginService::needCaptchaRevalidation();
    }
}
