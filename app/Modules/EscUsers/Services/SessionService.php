<?php

namespace App\Modules\EscUsers\Services;

use Illuminate\Support\Facades\Session;

class SessionService
{
    public static function setLogin($id)
    {
        return Session::set('Esc/Users/LoggedInUserId', $id);
    }

    public static function isLoggedIn()
    {
        return (int) Session::get('Esc/Users/LoggedInUserId');
    }

    public static function removeLogin()
    {
        return Session::flush();
    }

    public static function setNeedCatphaRevalition($val)
    {
        return Session::set('Esc/Users/needCatphaRevalition', $val);
    }

    public static function getNeedCatphaRevalition()
    {
        return (int) Session::get('Esc/Users/needCatphaRevalition');
    }

    public static function getLoggedInUserId()
    {
        return Session::get('Esc/Users/LoggedInUserId');
    }
}
