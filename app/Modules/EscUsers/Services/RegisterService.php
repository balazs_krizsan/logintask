<?php

namespace App\Modules\EscUsers\Services;

use App\Modules\EscUsers\Repositories\UsersRepository;
use App\Modules\EscUsers\Repositories\UsersEmailValidationRepository;

class RegisterService
{
    public static function createUser($formData)
    {
        $logger = \EscLog::getInstance('EscUsers::Services/RegisterService');
        if ($user = UsersRepository::create($formData)) {
            $logger->log(sprintf('Successful registration id#%d', $user->id));

            $hash = static::createEmailVerificationCode($user);
            static::sendVerificationMail($user, $hash);

            return $user;
        }
        $logger->log(sprintf('Registration failed with email%s: ', $formData['email']));
        return false;
    }

    private static function createEmailVerificationCode($user)
    {
        return UsersRepository::createEmailVerificationCode($user);
    }

    private static function sendVerificationMail($user, $hash)
    {
        \Mail::send(
            'EscUsers::emailTemplate.emailValidation', [
                'user' => $user, 'hash' => $hash, 'app_url' => env('APP_URL')
            ], function ($m) use ($user) {
                $m->from('testapp@domain.com', 'From Login');
                $m->to($user->email, $user->name)->subject('Login Test');
            }
        );
    }

    public static function validateEmail($userId, $hash)
    {
        $logger = \EscLog::getInstance('EscUsers::Services/RegisterService');
        try {
            $validation = UsersEmailValidationRepository::getByUserIdAndHash($userId, $hash);
            if ($validation->used) {
                $success = false;
                $message = 'This email address already registered';
            } else {
                $validation->used = 1;
                $validation->save();

                UsersRepository::activateEmail($userId);

                $success = true;
                $message = 'Successful e-mail activation';
            }
        } catch(\Exception $e) {
            $success = false;
            $message = 'Validation token not found';
        }

        $return = [
            'success' => $success,
            'message' => $message
        ];
        $logger->log(sprintf('email validation', $return));
        return $return;
    }
}
