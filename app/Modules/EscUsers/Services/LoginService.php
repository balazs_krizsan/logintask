<?php

namespace App\Modules\EscUsers\Services;

use App\Modules\EscUsers\Repositories\UsersRepository;
use App\Modules\EscUsers\Repositories\UsersLoginAttemptsRepository;
use App\Modules\EscUsers\Services\SessionService;

class LoginService
{
    public static function doLogin($email, $password)
    {
        $logger = \EscLog::getInstance('EscUsers::Services/LoginService');
        if ($user = UsersRepository::doLogin($email, $password)) {
            if ($user->activated) {
                SessionService::setLogin($user->id);

                // disable 2 step auth
                if (static::needCaptcha()) {
                    SessionService::setNeedCatphaRevalition(2);
                }

                $msg = sprintf('Login successful id#%d', $user->id);
                $logger->log($msg);

                return [
                    'success' => true,
                    'user'    => $user,
                ];
            }
            $msg = sprintf('Login failed with not validated id#: %d', $user->id);
            $logger->log($msg);

            return [
                'success' => false,
                'error'   => ['email' => 'This e-mail address is not validated yet.'],
            ];
        }
        $msg = sprintf('Login failed with mail: %s', $email);
        $logger->log($msg);

        return [
            'success' => false,
            'error'   => ['password' => 'Incorrect email and password combination.'],
        ];
    }

    public static function logFailedAttempt($email, $ip)
    {
        UsersLoginAttemptsRepository::logEmail($email, $ip);
        UsersLoginAttemptsRepository::logIp($ip);
        if (!static::needCaptcha()) {
            UsersLoginAttemptsRepository::logIpNetwork(IpService::ipToMaskedNetworkIp($ip, 16), $ip);
            UsersLoginAttemptsRepository::logIpNetwork(IpService::ipToMaskedNetworkIp($ip, 24), $ip);
        }
    }

    public static function isLoggedIn()
    {
        return SessionService::isLoggedIn();
    }

    public static function isLoggedInForView()
    {
        if (SessionService::getNeedCatphaRevalition() == 1) {
            return false;
        }
        return SessionService::isLoggedIn();
    }

    public static function doLogout()
    {
        SessionService::removeLogin();
    }

    public static function needCaptcha()
    {
        $ip = request()->ip();

        if (UsersLoginAttemptsRepository::numByIp($ip) > 3) return true;

        $ipNetwork24 = IpService::ipToMaskedNetworkIp($ip, 24);
        if (UsersLoginAttemptsRepository::numByIpNetwork($ipNetwork24) > 500) return true;

        $ipNetwork16 = IpService::ipToMaskedNetworkIp($ip, 16);
        if (UsersLoginAttemptsRepository::numByIpNetwork($ipNetwork16) > 1000) return true;

        return false;
    }

    public static function needCaptchaRevalidation()
    {
        // 0 = not exists session var = need check
        // 1 = exists session var = need captcha retype
        // 2 = exists session var = check not needed
        $needRevalidate = SessionService::getNeedCatphaRevalition();

        if ($needRevalidate == 2) {
            return false;
        }
        if ($needRevalidate == 0) {
            $current = \EscUsers::getCurrent();
            $numEmail = UsersLoginAttemptsRepository::numByEmail($current->email);
            if ($numEmail <= 3) {
                SessionService::setNeedCatphaRevalition(2);
                return false;
            }
            SessionService::setNeedCatphaRevalition(1);
        }
        // $needRevalidate == 1
        return true;
    }

    public static function captchaRavlidationOk()
    {
        SessionService::setNeedCatphaRevalition(2);
    }
}
