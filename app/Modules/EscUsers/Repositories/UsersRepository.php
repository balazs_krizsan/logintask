<?php

namespace App\Modules\EscUsers\Repositories;

use App\Modules\EscUsers\Models\Users;
use App\Modules\EscUsers\Models\UsersEmailValidation;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class UsersRepository
{
    public static function create(array $userData)
    {
        $allowed = array('email', 'name', 'password');

        $userData['password'] = password_hash($userData['password'], PASSWORD_DEFAULT, ['cost' => 12]);

        return Users::create(array_intersect_key($userData, array_flip($allowed)));
    }

    public static function doLogin($email, $password)
    {
        try {
            $user = Users::where('email', $email)->firstorfail();
            if (password_verify($password, $user->password)) {
                return $user;
            }
            return false;
        } catch(ModelNotFoundException $e) {
            \EscLog::getInstance('EscUsers::Repositories/UserRepository')->error('login error', $e->getMessage());
        }
        return false;
    }

    public static function createEmailVerificationCode($user)
    {
        $hash = sha1(microtime().rand(1, 999999));
        UsersEmailValidation::create(['user_id' => $user->id, 'hash' => $hash]);
        return $hash;
    }

    public static function activateEmail($userId)
    {
        $user = Users::find($userId);
        $user->activated = 1;
        $user->save();
    }
}
