<?php

namespace App\Modules\EscUsers\Repositories;

use App\Modules\EscUsers\Models\UsersEmailValidation;

class UsersEmailValidationRepository
{
    public static function createEmailVerificationCode($user)
    {
        $hash = sha1(microtime().rand(1, 999999));
        UsersEmailValidation::create(['user_id' => $user->id, 'hash' => $hash]);
        return $hash;
    }

    public static function getByUserIdAndHash($userId, $hash)
    {
        return UsersEmailValidation::where('user_id', $userId)->where('hash', $hash)->firstOrFail();
    }
}
