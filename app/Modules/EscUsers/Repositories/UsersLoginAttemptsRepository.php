<?php

namespace App\Modules\EscUsers\Repositories;

use App\Modules\EscUsers\Models\EscUsersEmailValidation as UsersEmailValidationModel;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Modules\EscUsers\Models\LoginAttempts;

class UsersLoginAttemptsRepository
{
    const LOG_TYPE_IP         = 1;
    const LOG_TYPE_EMAIL      = 2;
    const LOG_TYPE_IP_NETWORK = 3;

    public static function logIp($ip)
    {
        static::log(static::LOG_TYPE_IP, $ip, $ip);
    }

    public static function logIpNetwork($ipNetwork, $ip)
    {
        static::log(static::LOG_TYPE_IP_NETWORK, $ipNetwork, $ip);
    }

    public static function logEmail($email, $ip)
    {
        static::log(static::LOG_TYPE_EMAIL, $email, $ip);
    }

    public static function log($logType, $data, $ip)
    {
        if ($id = static::getLast300sLogId($logType, $data)) {
            return static::incrementLogAttempts($id);
        }
        return static::insertLog($logType, $data, $ip);
    }

    private static function insertLog($type, $data, $ip)
    {
        return LoginAttempts::create(['data' => $data, 'log_type' => $type, 'request_address' => $ip]);
    }

    private static function incrementLogAttempts($id)
    {
        $attempt = LoginAttempts::find($id);
        $attempt->attempts++;
        return $attempt->save();
    }

    public static function getLast300sLogId($type, $data)
    {
        try {
            $attempt = LoginAttempts::
                where('log_type', $type)
                ->where('data', $data)
                ->where('created_at', '>', \DB::raw('NOW() - INTERVAL \'5MINS\''))
                ->firstOrFail();
            return $attempt->id;
        } catch (ModelNotFoundException $e) {
            return false;
        }
    }

    public static function numByIpNetwork($ipNetwork)
    {
        $res = \DB::table(LoginAttempts::$tableStatic)
            ->select(\DB::raw('sum(attempts) as total'))
            ->where('data', $ipNetwork)
            ->where('log_type', static::LOG_TYPE_IP_NETWORK)
            ->where('created_at', '>', \DB::raw('NOW() - INTERVAL \'60MINS\''))
            ->groupBy('request_address')
            ->first();
        if (!$res) return 0;
        return $res->total;
    }

    public static function numByIp($ip)
    {
        $res = \DB::table(LoginAttempts::$tableStatic)
            ->select(\DB::raw('sum(attempts) as total'))
            ->where('request_address', $ip)
            ->where('log_type', static::LOG_TYPE_IP)
            ->where('created_at', '>', \DB::raw('NOW() - INTERVAL \'60MINS\''))
            ->groupBy('request_address')
            ->first();
        if (!$res) return 0;
        return $res->total;
    }

    public static function numByEmail($email)
    {
        $res = \DB::table(LoginAttempts::$tableStatic)
            ->select(\DB::raw('sum(attempts) as total'))
            ->where('data', $email)
            ->where('log_type', static::LOG_TYPE_EMAIL)
            ->where('created_at', '>', \DB::raw('NOW() - INTERVAL \'60MINS\''))
            ->groupBy('request_address')
            ->first();
        if (!$res) return 0;
        return $res->total;
    }

}
