<?php

Route::group([
    'middleware' => ['web'],
    'namespace' => 'App\Modules\EscUsers\Controllers',
    'prefix' => 'users',
    'as' => 'Users',
], function() {

    Route::get('/login', 'LoginController@showLoginForm');
    Route::post('/login', 'LoginController@postLogin');
    Route::get('/logout', 'LoginController@getLogout');
    Route::post('/captcha-revalidation', 'LoginController@postCaptchaRevalidation');
    Route::get('/captcha-revalidation', 'LoginController@getCaptchaRevalidation');

    Route::get('/registration', 'RegistrationController@getRegistration');
    Route::post('/registration', 'RegistrationController@postRegistration');
    Route::get('/email-validation', 'RegistrationController@getEmailValidation');

});
