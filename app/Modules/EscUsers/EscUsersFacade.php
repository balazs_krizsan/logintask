<?php

namespace App\Modules\EscUsers;

use Illuminate\Support\Facades\Facade;

class EscUsers extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'escusers';
    }
}
