<?php

namespace App\Modules\EscUsers;

use Illuminate\Support\Facades\Facade;
use Validator;

class Bootstrap {
    public function init()
    {
        // The password length must be greater than or equal to 8
        // The password must contain one or more uppercase characters
        // The password must contain one or more lowercase characters
        // The password must contain one or more numeric values
        // The password must contain one or more special characters
        Validator::extend('strongpassword', function($attribute, $value, $parameters, $validator) {
            return preg_match(
                '/(?=^.{8,}$)(?=.*\d)(?=.*[!@#$%^&*]+)(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/',
                $value
            );
        });
        Validator::replacer('strongpassword', function($message, $attribute, $rule, $parameters) {
            return "The password length must be greater than or equal to 8 |
                The password must contain one or more uppercase characters |
                The password must contain one or more lowercase characters |
                The password must contain one or more numeric values |
                The password must contain one or more special characters";
        });
    }
}
