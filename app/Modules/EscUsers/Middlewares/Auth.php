<?php

namespace App\Modules\EscUsers\Middlewares;

use Closure;

class Auth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (!\EscUsers::isLoggedIn()) {
                return redirect(\EscUsers::getUrl('login'));
        }
        if (\EscUsers::needCaptchaRevalidation()) {
            return redirect(\EscUsers::getUrl('captchaRevalidation'));
        }

        return $next($request);
    }
}
