<?php

namespace App\Modules\EscUsers\Models;

use Illuminate\Database\Eloquent\Model;

class LoginAttempts extends Model
{
    protected $guarded = ['id'];
    protected $table = 'esc_users_login_attempts';
    public static $tableStatic = 'esc_users_login_attempts';
}
