<?php

namespace App\Modules\EscUsers\Models;

use Illuminate\Database\Eloquent\Model;

class UsersEmailValidation extends Model
{
    protected $fillable = ['user_id', 'hash'];
    protected $table = 'esc_users_email_validation';
}
