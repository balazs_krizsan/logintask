<?php

namespace App\Modules\EscUsers\Models;

use Illuminate\Database\Eloquent\Model;

class Users extends Model
{
    protected $table = 'esc_users';
    protected $guarded = ['id'];
}
