@extends('layouts.site')

@section('content')
    @if ($response['success'])
        E-mail address is validated!
    @else
        Address validation error: {{ $response['message'] }}
    @endif
@endsection
