@extends('layouts.site')

@section('content')

<div class="row">
    <div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3">
        <div class="card card-signup">
            {!! Form::open(['url' => '/users/registration', 'method' => 'post']) !!}
            <div class="header header-primary text-center">
                <h4>Login</h4>
            </div>
            <div class="content">

                <div class="col-sm-12">
                    <div class="form-group label-floating{{ $errors->has('email') ? ' has-error' : '' }}">
                        <div class="input-group" >
                                <span class="input-group-addon">
                                    <i class="material-icons">group</i>
                                </span>
                            {!! Form::label('E-mail', '', ['class' => 'control-label']) !!}
                            {!! Form::email('email', '', ['class' => 'form-control']) !!}
                        </div>
                    </div>
                </div>
                @if ($errors->has('email'))
                    <div class="col-sm-12">
                        <div class="alert alert-danger">
                            <div class="container-fluid">
                                <div class="alert-icon">
                                    <i class="material-icons">error_outline</i>
                                </div>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true"><i class="material-icons">clear</i></span>
                                </button>
                                {{ $errors->first('email') }}
                            </div>
                        </div>
                    </div>
                @endif

                <div class="col-sm-12">
                    <div class="form-group label-floating{{ $errors->has('name') ? ' has-error' : '' }}">
                        <div class="input-group" >
                                <span class="input-group-addon">
                                    <i class="material-icons">group</i>
                                </span>
                            {!! Form::label('Name', '', ['class' => 'control-label']) !!}
                            {!! Form::text('name', '', ['class' => 'form-control']) !!}
                        </div>
                    </div>
                </div>
                @if ($errors->has('name'))
                    <div class="col-sm-12">
                        <div class="alert alert-danger">
                            <div class="container-fluid">
                                <div class="alert-icon">
                                    <i class="material-icons">error_outline</i>
                                </div>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true"><i class="material-icons">clear</i></span>
                                </button>
                                {{ $errors->first('name') }}
                            </div>
                        </div>
                    </div>
                @endif

                <div class="col-sm-12">
                    <div class="form-group label-floating{{ $errors->has('password') ? ' has-error' : '' }}">
                        <div class="input-group" >
                                <span class="input-group-addon">
                                    <i class="material-icons">group</i>
                                </span>
                            {!! Form::label('Password', '', ['class' => 'control-label']) !!}
                            {!! Form::password('password', ['class' => 'form-control']) !!}
                        </div>
                    </div>
                </div>
                @if ($errors->has('password'))
                    <div class="col-sm-12">
                        <div class="alert alert-danger">
                            <div class="container-fluid">
                                <div class="alert-icon">
                                    <i class="material-icons">error_outline</i>
                                </div>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true"><i class="material-icons">clear</i></span>
                                </button>
                                {{ $errors->first('password') }}
                            </div>
                        </div>
                    </div>
                @endif

                <div class="col-sm-12">
                    <div class="form-group label-floating{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                        <div class="input-group" >
                                <span class="input-group-addon">
                                    <i class="material-icons">group</i>
                                </span>
                            {!! Form::label('Password confirmation', '', ['class' => 'control-label']) !!}
                            {!! Form::password('password_confirmation', ['class' => 'form-control']) !!}
                        </div>
                    </div>
                </div>
                @if ($errors->has('password_confirmation'))
                    <div class="col-sm-12">
                        <div class="alert alert-danger">
                            <div class="container-fluid">
                                <div class="alert-icon">
                                    <i class="material-icons">error_outline</i>
                                </div>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true"><i class="material-icons">clear</i></span>
                                </button>
                                {{ $errors->first('password_confirmation') }}
                            </div>
                        </div>
                    </div>
                @endif
                <div class="footer text-center">
                    {{ Form::submit('Register', ['class' => "btn btn-simple btn-primary btn-lg"]) }}
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
@endsection
