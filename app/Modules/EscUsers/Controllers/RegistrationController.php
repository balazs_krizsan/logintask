<?php

namespace App\Modules\EscUsers\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Validator;

use \App\Modules\EscUsers\Services\RegisterService;

class RegistrationController extends Controller
{
    public function getRegistration()
    {
        return view('EscUsers::registration.getRegistration');
    }

    public function postRegistration(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email'  => 'required|email|unique:esc_users',
            'name' => 'required|string|min:3|unique:esc_users',
            'password' => 'required|strongpassword',
            'password_confirmation' => 'required|same:password',
        ]);
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }

        RegisterService::createUser($request->all());

        return redirect('/users/login');
    }

    public function getEmailValidation(Request $request)
    {
        $response = RegisterService::validateEmail($request->get('user_id'), $request->get('hash'));
        return view('EscUsers::registration.getEmailValidation', ['response' => $response]);
    }
}
