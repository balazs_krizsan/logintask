<?php

namespace App\Modules\EscUsers\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Validator;

use \App\Modules\EscUsers\Services\LoginService;
use \App\Modules\EscUsers\Services\SessionService;

class LoginController extends Controller
{
    public function showLoginForm(Request $request)
    {
        return view('EscUsers::login.loginForm', ['needCaptcha' => \EscUsers::needCaptcha()]);
    }

    public function postLogin(Request $request)
    {
        $rules = [
            'email'  => 'required|email',
            'password' => 'required|strongpassword',
        ];
        if (\EscUsers::needCaptcha()) {
            $rules['g-recaptcha-response'] = 'required|recaptcha';
        }
        $validator = Validator::make($request->all(), $rules);

        $email    = $request->input('email');
        $password = $request->input('password');
        $ip       = $request->ip();

        if ($validator->fails()) {
            LoginService::logFailedAttempt($email, $ip);
            return back()->withErrors($validator)->withInput();
        }
        $response = LoginService::doLogin($email, $password);
        if ($response['success']) {
            return redirect('/home');
        }

        LoginService::logFailedAttempt($email, $ip);
        return back()->withInput()->withErrors($response['error']);
    }

    public function getLogout()
    {
        LoginService::doLogout();
        return redirect('/');
    }

    public function getCaptchaRevalidation()
    {
        return view('EscUsers::login.getCaptchaRevalidation');
    }

    public function postCaptchaRevalidation(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'g-recaptcha-response' => 'required|recaptcha'
        ]);
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }

        LoginService::captchaRavlidationOk();

        return redirect('/home');
    }
}
