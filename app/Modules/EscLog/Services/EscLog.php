<?php

namespace App\Modules\EscLog\Services;

class EscLog
{
    public static function getInstance($scope)
    {
        return new Logger($scope);
    }
}

class Logger {
    public function __construct($scope)
    {
        $this->scope = $scope;
    }

	public function log($message, $data = null)
    {
		$this->write('message('.$message.')', $data);
	}

	public function warn($message, $data = null)
    {
		$this->write('warn('.$message.')', $data);
	}

	public function error($message, $data = null)
    {
		$this->write('error('.$message.')', $data);
	}

	public function write($message, $data) {
        $finalMessage = PHP_EOL;

		if ($this->scope) {
            $finalMessage .= 'scope('.$this->scope.') ';
		}

		$finalMessage .= $message;

        if ($data) {
			$finalMessage .= ' data('.preg_replace('/\s\s+/', ' ', json_encode($data)).')';
		}

		file_put_contents('/vagrant/shared/docler/logs/main.log', $finalMessage, FILE_APPEND);
	}
}
