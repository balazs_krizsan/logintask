<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Services\Tasks as TasksServices;

class TasksRemoveOld extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'tasks:removeold';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Remove old not completed tasks';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $service = new TasksServices();
        $service->removeOlds();
    }
}
