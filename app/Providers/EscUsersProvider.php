<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App;

class EscUsersProvider extends ServiceProvider {
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        App::bind('escusers', function()
        {
            return new \App\Modules\EscUsers\Services\EscUsers;
        });
    }
}
