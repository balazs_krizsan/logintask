<?php namespace App\Modules;


namespace App\Providers;

use Illuminate\Contracts\Events\Dispatcher as DispatcherContract;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class ModulesServiceProvider extends ServiceProvider
{

    public function boot(DispatcherContract $events)
    {
        parent::boot($events);
        $modules = config("modules.modules");

        while (list(,$module) = each($modules)) {

            $routersPath = __DIR__.'/../Modules/'.$module.'/routes.php';
            if(file_exists($routersPath)) {
                include $routersPath;
            }

            $bootstrapPath = __DIR__.'/../Modules/'.$module.'/Bootstrap.php';
            if(is_file($bootstrapPath)) {
                $bootstrap = new \App\Modules\EscUsers\Bootstrap;
                $bootstrap->init();
            }

            $viewsPath = __DIR__.'/../Modules/'.$module.'/Views';
            if(is_dir($viewsPath)) {
                $this->loadViewsFrom($viewsPath, $module);
            }
        }
    }

    public function register() {}

}
