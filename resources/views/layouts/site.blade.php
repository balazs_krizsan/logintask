<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <link rel="apple-touch-icon" sizes="76x76" href="img/apple-icon.png">
    <link rel="icon" type="image/png" href="img/favicon.png">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

    <title>Test</title>

    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />

    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons" />
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />

    <link href="/css/bootstrap.min.css" rel="stylesheet" />
    <link href="/sass/material-kit.css" rel="stylesheet"/>
</head>
<body>
<div class="wrapper">
    <div class="header">
        <nav class="navbar navbar-danger">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#example-navbar-danger">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">Test</a>
                </div>

                <div class="collapse navbar-collapse" id="example-navbar-danger">
                    <ul class="nav navbar-nav navbar-right">
                        @if (\EscUsers::isLoggedInForView())
                            <li><a href="/home">Home</a></li>
                            <li><a href="{{ \EscUsers::getUrl('logout') }}">Logout</a></li>
                        @else
                            <li><a href="/">Home</a></li>
                            <li><a href="{{ \EscUsers::getUrl('login') }}">Login</a></li>
                            <li><a href="{{ \EscUsers::getUrl('registration') }}">Registration</a></li>
                        @endif
                    </ul>
                </div>
            </div>
        </nav>
    </div>

    <div class="main">
        @yield('content')
    </div>

    <footer class="footer">
        <div class="container">
            <nav class="pull-left">
                <ul>
                    <li><a href="{{ \EscUsers::getUrl('login') }}">Login</a></li>
                    <li><a href="{{ \EscUsers::getUrl('registration') }}">Registration</a></li>
                </ul>
            </nav>
            <div class="copyright pull-right">
                &copy; 2016, <a href="#">Krizsan Balazs</a>
            </div>
        </div>
    </footer>
</div>
</body>
<script src="/js/jquery.min.js" type="text/javascript"></script>
<script src="/js/bootstrap.min.js" type="text/javascript"></script>
<script src="/js/material.min.js"></script>
<script src="/js/nouislider.min.js" type="text/javascript"></script>
<script src="/js/bootstrap-datepicker.js" type="text/javascript"></script>
<script src="/js/material-kit.js" type="text/javascript"></script>
</html>
