@extends('layouts.site')

@section('content')
    {!! Form::open(['url' => '/tasks/post', 'method' => 'post']) !!}

        <div class="{{ $errors->has('taskdata') ? ' has-error' : '' }}">
            {!! Form::label('Task JSON data') !!}
            <br />
            {!! Form::textarea('taskdata', '') !!}

            @if ($errors->has('taskdata'))
                <div>{{ $errors->first('taskdata') }}</div>
            @endif
        </div>

        {{ Form::submit() }}
    {!! Form::close() !!}
@endsection
