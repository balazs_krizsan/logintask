@extends('layouts.site')

@section('content')
    <div class="row">
        <div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3">
            {!! Form::open(['url' => '/tasks/attempt', 'method' => 'post']) !!}
                {!! Form::hidden('hash', $hash) !!}
                <div class="{{ $errors->has('query') ? ' has-error' : '' }}">
                    {!! Form::label('Query:') !!}
                    {!! Form::textarea('query', '', ['class' => 'form-control']) !!}
                    @if ($errors->has('query'))
                        <div>{{ $errors->first('query') }}</div>
                    @endif
                </div>
                {{ Form::submit() }}
            {!! Form::close() !!}
        </div>
    </div>
@endsection
