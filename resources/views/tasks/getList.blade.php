@extends('layouts.site')

@section('content')
    <ul>
        @foreach ($tasks as $task)
            <li>
                <a href="{{ $task->getUrl('fill') }}">Try this</a>
                <br />
                {{ $task->taskdata }}
            </li>
        @endforeach
    </ul>
    <a href="{{ $tasks->previousPageUrl() }}">prev</a>
    <a href="{{ $tasks->nextPageUrl() }}">next</a>
@endsection
